# js-yarn

Test project with:

* **Language:** Javascript
* **Package Manager:** Yarn

Please see the [usage documentation](https://gitlab.com/gitlab-org/security-products/tests/common#how-to-use) for Security Products test projects.

## Supported Security Products Features

| Feature             | Supported          |
|---------------------|--------------------|
| SAST                | :x:                |
| Dependency Scanning | :white_check_mark: |
| Container Scanning  | :x:                |
| DAST                | :x:                |
| License Management  | :white_check_mark: |